var grpc = require('@grpc/grpc-js');
var protoLoader = require('@grpc/proto-loader');

var PROTO_PATH = './src/protos/greeter.proto';

function greet() {

  var target = 'localhost:5035';
  var user = 'FROM THE OTHER SIDE'

  var packageDefinition = protoLoader.loadSync(
    PROTO_PATH,
    {keepCase: true,
     longs: String,
     enums: String,
     defaults: true,
     oneofs: true
    });

  var hello_proto = grpc.loadPackageDefinition(packageDefinition).greet; // package name
  var gRPC_Client = new hello_proto.Greeter(target, grpc.credentials.createInsecure());

  gRPC_Client.sayHello({name: user}, function(err, response) {
    console.log(response.message);
  });
}


function greetAsync() {

  var target = 'localhost:5035';
  var user = 'FROM THE OTHER SIDE ASYNCHRONOUSLY'
  
  protoLoader.load(PROTO_PATH, {
    keepCase: true,
    longs: String,
    enums: String,
    defaults: true,
    oneofs: true}).then(packageDefinition => {

      const packageObject = grpc.loadPackageDefinition(packageDefinition);
      const greeter_proto = packageObject.greet
      const GRPC_CLIENT = new greeter_proto.Greeter(target, grpc.credentials.createInsecure());

      return GRPC_CLIENT.sayHello({name: user}, function(err, response) {
        console.log(response.message);
      });
    })
}



greet();
greetAsync()